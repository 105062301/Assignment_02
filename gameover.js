var slickUI;
var gameoverState = {
    preload: function(){
        slickUI = game.plugins.add(Phaser.Plugin.SlickUI);
        slickUI.load('assets/ui/kenney/kenney.json');
    },
    create: function() {
        this.bg = game.add.image(0, 0, 'background');
        this.bg.scale.setTo(0.4, 0.3);
        this.explosionSound = game.add.audio('explosion');
        this.explosionSound.play();
        this.button = game.add.button(game.width/2-70, game.height/2+190, 'button', this.replay, this, 2, 1, 0);
        this.button.scale.setTo(0.4, 0.4);
        var buttonLabel = game.add.text(game.width/2-60, game.height/2+190, 'Restart', { font: '25px Arial', fill: 'white' });

        this.button = game.add.button(game.width/2-70, game.height/2+130, 'button', this.menu, this, 2, 1, 0);
        this.button.scale.setTo(0.4, 0.4);
        var buttonLabel = game.add.text(game.width/2-50, game.height/2+130, ' Quit', { font: '25px Arial', fill: 'white' });
        var panel;
        slickUI.add(panel = new SlickUI.Element.Panel(50, game.height/2+50, 400, 70));
        //panel.add(new SlickUI.Element.Text(10,10, "Text input")).centerHorizontally().text.alpha = 0.5;
        panel.add(new SlickUI.Element.Text(12,0, "Type your name for ranking!"));
        var textField = panel.add(new SlickUI.Element.TextField(10,20, panel.width - 20, 40));
        textField.value = ''
        textField.events.onOK.add(function () {
            alert('Your name is: ' + textField.value);
            game.global.name = textField.value;
            game.global.eat = 1;
            game.state.start('ranking');

        });
        textField.events.onToggle.add(function (open) {
            console.log('You just ' + (open ? 'opened' : 'closed') + ' the virtual keyboard');
        });
        textField.events.onKeyPress.add(function(key) {
            console.log('You pressed: ' + key);
        });

        // Add a background image
        this.bg = game.add.image(game.width/2-120, game.height/2-230, 'ggbackground');
        this.bg.scale.setTo(1,1);
        // Display the name of the game
        
        // Show the score at the center of the screen
        if(game.global.p1win == 1){
            var scoreLabel = game.add.text(game.width/2, game.height/2+20,
            'P1 wins! score: B' + game.global.score, { font: '30px Arial', fill: 'red' });
            scoreLabel.anchor.setTo(0.5, 0.5);
        }
        else if(game.global.p1win==0){
            var scoreLabel = game.add.text(game.width/2, game.height/2+20,
            'P2 wins! score: B' + game.global.score, { font: '30px Arial', fill: 'red' });
            scoreLabel.anchor.setTo(0.5, 0.5);
        }
        else{
            var scoreLabel = game.add.text(game.width/2, game.height/2+20,
            'score: B' + game.global.score, { font: '30px Arial', fill: 'red' });
            scoreLabel.anchor.setTo(0.5, 0.5);
        }
        // Explain how to start the game

        //var input = game.add.inputField(game.width/2, game.height/2+70);
        /*var password = game.add.inputField(10, 90, {
            font: '18px Arial',
            fill: '#212121',
            fontWeight: 'bold',
            width: 150,
            padding: 8,
            borderWidth: 1,
            borderColor: '#000',
            borderRadius: 6,
            placeHolder: 'Password',
            type: Fabrique.InputType.password
        });*/
    
        

        //var startLabel = game.add.text(game.width/2, game.height-80, 'press enter key to play again', { font: '25px Arial', fill: '#ffffff' });
        //startLabel.anchor.setTo(0.5, 0.5);
        // Create a new Phaser keyboard variable: the up arrow key
        // When pressed, call the 'start'
        //var enterKey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
        //enterKey.onDown.add(this.start, this);
        },
        menu: function() {
            game.global.score = 0;
            game.state.start('menu');
        },
        replay: function(){
            game.global.score = 0;
            if(game.global.p1win<2){
                game.state.start('play2');
            }
            else{
                game.state.start('play');
            }      
            
        }
}; 



