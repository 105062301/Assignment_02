var play2State = {
    preload: function() {
        game.load.spritesheet('player2', 'assets/player.png', 32, 32);
    }, // The proload state does nothing now.
    create: function() {
    // Removed background color, physics system, and roundPixels.
    // replace 'var score = 0' by global score variable.
    this.bg = game.add.image(0, 0, 'night');
    keyboard = game.input.keyboard.addKeys({
        'up': Phaser.Keyboard.UP,
        'down': Phaser.Keyboard.DOWN,
        'left': Phaser.Keyboard.LEFT,
        'right': Phaser.Keyboard.RIGHT,
        'w': Phaser.Keyboard.W,
        'a': Phaser.Keyboard.A,
        's': Phaser.Keyboard.S,
        'd': Phaser.Keyboard.D
    });
    
    boundary();
    player2make();
    text2make();
    game.global.score = 0;
    this.healSound = game.add.audio('heal');
    bgmusicSound = game.add.audio('bgmusic');
    bgmusicSound.play();
    bgmusicSound.loop = true;
    
    this.walls = game.add.group();
    
    },
    update: function() {

        this.physics.arcade.collide(player, platforms, effect);
        this.physics.arcade.collide(player, [leftWall, rightWall]);
        this.physics.arcade.collide(player2, platforms, effect2);
        this.physics.arcade.collide(player2, [leftWall, rightWall]);
        this.physics.arcade.collide(player2, player);

        this.physics.arcade.overlap(player, firstads, takeFirstad, null, this);
        this.physics.arcade.overlap(player2, firstads, takeFirstad2, null, this);
        if(player.body.y < 0) {
            if(player.body.velocity.y < 0) {
                player.body.velocity.y = 0;
            }
            if(game.time.now > player.unbeatableTime) {
                this.hurtSound = game.add.audio('hurt');
                this.hurtSound.play();
                player.life -= 3;
                game.camera.flash(0xff0000, 100);
                player.unbeatableTime = game.time.now + 2000;
            }
        }
        if(player2.body.y < 0) {
            if(player2.body.velocity.y < 0) {
                player2.body.velocity.y = 0;
            }
            if(game.time.now > player2.unbeatableTime) {
                this.hurtSound = game.add.audio('hurt');
                this.hurtSound.play();
                player2.life -= 3;
                game.camera.flash(0xff0000, 100);
                player2.unbeatableTime = game.time.now + 2000;
            }
        }
        checkGameOver2();

        player2update();
        updatePlatforms();
        text2update();

        createPlatforms();
        createFirstad();
        updateFirstad();
    }, // No changes
    
};
        // Delete all Phaser initialization code

var player, player2;
var keyboard;
var lastTime = 0;
var lasttime2 = 0;

var platforms = [];
var firstads = [];

var leftWall; 
var ceiling;

var text1;
var text2;
var text3;
var text4;
var text5;



var distance = 0;


function takeFirstad2(player2, firstad) {
    this.healSound.play();
    firstad.kill(); 
    player2.life += 3;
    if(player2.life>10){
        player2.life = 10;
    }
}


function player2make() {
    
    player = game.add.sprite(230, 50, 'player');
    player.direction = 10;
    game.physics.arcade.enable(player);
    player.body.gravity.y = 500;
    player.animations.add('left', [0, 1, 2, 3], 8);
    player.animations.add('right', [9, 10, 11, 12], 8);
    player.animations.add('flyleft', [18, 19, 20, 21], 12);
    player.animations.add('flyright', [27, 28, 29, 30], 12);
    player.animations.add('fly', [36, 37, 38, 39], 12);
    player.life = 10;
    player.unbeatableTime = 0;
    player.touchOn = undefined;

    var platform = game.add.sprite(200, 300, 'stone');
    game.physics.arcade.enable(platform);
    platform.body.immovable = true;
    platforms.push(platform);
    platform.body.checkCollision.down = false;
    platform.body.checkCollision.left = false;
    platform.body.checkCollision.right = false;


    player2 = game.add.sprite(200, 50, 'player');
    player2.direction = 10;
    game.physics.arcade.enable(player2);
    player2.body.gravity.y = 500;
    player2.animations.add('left', [0, 1, 2, 3], 8);
    player2.animations.add('right', [9, 10, 11, 12], 8);
    player2.animations.add('flyleft', [18, 19, 20, 21], 12);
    player2.animations.add('flyright', [27, 28, 29, 30], 12);
    player2.animations.add('fly', [36, 37, 38, 39], 12);
    player2.life = 10;
    player2.unbeatableTime = 0;
    player2.touchOn = undefined;
}

function text2make () {
    var style = {fill: '#ff0000', fontSize: '20px'}
    text1 = game.add.text(10, 10, '', style);
    text2 = game.add.text(game.width-50, 10, '', style);
    text3= game.add.text(100, 10, '', style);
    text4= game.add.text(player.body.x, player.body.y, 'P1', style);
    text5= game.add.text(player2.body.x, player.body.y, 'P2', style);
}

function player2update () {
    if(keyboard.left.isDown) {
        player.body.velocity.x = -250;
    } else if(keyboard.right.isDown) {
        player.body.velocity.x = 250;
    } else {
        player.body.velocity.x = 0;
    }
    setPlayerAnimate(player);

    if(keyboard.a.isDown) {
        player2.body.velocity.x = -250;
    } else if(keyboard.d.isDown) {
        player2.body.velocity.x = 250;
    } else {
        player2.body.velocity.x = 0;
    }
    setPlayerAnimate2(player2);
}

function setPlayerAnimate2(player2) {
    var x = player2.body.velocity.x;
    var y = player2.body.velocity.y;

    if (x < 0 && y > 0) {
        player2.animations.play('flyleft');
    }
    if (x > 0 && y > 0) {
        player2.animations.play('flyright');
    }
    if (x < 0 && y == 0) {
        player2.animations.play('left');
    }
    if (x > 0 && y == 0) {
        player2.animations.play('right');
    }
    if (x == 0 && y != 0) {
        player2.animations.play('fly');
    }
    if (x == 0 && y == 0) {
      player2.frame = 8;
    }
}


function text2update () {
    text1.setText('P1: ' + player.life);
    text2.setText('B' + distance);
    text3.setText('P2: ' + player2.life);
    text4.position.x = player.body.x;
    text4.position.y = player.body.y-40;
    text5.position.x = player2.body.x;
    text5.position.y = player2.body.y-40;
}



function effect2(player2, platform) {
    if(platform.key == 'moving') {
        if (player2.touchOn !== platform) {
            player2.touchOn = platform;
        }
    }
    if(platform.key == 'conveyorRight') {
        player2.body.x += 2;
    }
    if(platform.key == 'conveyorLeft') {
        player2.body.x -= 2;
    }
    if(platform.key == 'trampoline') {
        platform.animations.play('jump');
        this.jumpSound = game.add.audio('jump');
        this.jumpSound.play();
        player2.body.velocity.y = -350;
    }
    if(platform.key == 'nails') {
        if (player2.touchOn !== platform) {
            this.hurtSound = game.add.audio('hurt');
            this.hurtSound.play();
            player2.life -= 3;
            player2.touchOn = platform;
            game.camera.flash(0xff0000, 100);
        }
    }
    if(platform.key == 'normal') {
        if (player2.touchOn !== platform) {
            player2.touchOn = platform;
        }
    }
    if(platform.key == 'fake') {
        if(player2.touchOn !== platform) {
            platform.animations.play('turn');
            setTimeout(function() {
                platform.body.checkCollision.up = false;
            }, 100);
            player2.touchOn = platform;
        }
    }
}


function checkGameOver2 () {
    if(player.life <= 0 || player.body.y > game.height+150) {
        game.global.p1win = 0;
        gameOver2();
    }
    else if(player2.life <= 0 || player2.body.y > game.height+150) {
        game.global.p1win = 1;
        gameOver2();
    }
}

function gameOver2 () {
    bgmusicSound.stop();
    this.dieSound = game.add.audio('die');
    this.dieSound.play();
    platforms.forEach(function(s) {s.destroy()});
    platforms = [];
    firstads.forEach(function(s) {s.destroy()});
    firstads = [];
    game.global.score = distance;
    distance = 0;
    game.state.start('gameover');
}






