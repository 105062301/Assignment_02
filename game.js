
// Initialize Phaser

var game = new Phaser.Game(500, 600, Phaser.AUTO, 'canvas');
// Define our global variable
game.global = { score: 0, p1win: 2, name:'', eat: 0, shake: 0};
// Add all the states
game.state.add('boot', bootState);
game.state.add('load', loadState);
game.state.add('menu', menuState);
game.state.add('play', playState);
game.state.add('play2', play2State);
game.state.add('gameover', gameoverState);
game.state.add('ranking', rankingState);
// Start the 'boot' state
game.state.start('boot');