var playState = {
    preload: function() {
        game.global.p1win = 2;
    }, 
    create: function() {
   
    this.bg = game.add.image(0, 0, 'night');
    //this.bg.scale.setTo(0.4, 0.3);
    keyboard = game.input.keyboard.addKeys({
        'up': Phaser.Keyboard.UP,
        'down': Phaser.Keyboard.DOWN,
        'left': Phaser.Keyboard.LEFT,
        'right': Phaser.Keyboard.RIGHT,
        'w': Phaser.Keyboard.W,
        'a': Phaser.Keyboard.A,
        's': Phaser.Keyboard.S,
        'd': Phaser.Keyboard.D
    });
    boundary();
    playerMake();
    text1make();
    game.global.score = 0;
    this.healSound = game.add.audio('heal');
    bgmusicSound = game.add.audio('bgmusic');
    bgmusicSound.play();
    
    // The following part is the same as in previous lecture.
    this.walls = game.add.group();
    
    },
    update: function() {

        this.physics.arcade.collide(player, platforms, effect);
        this.physics.arcade.collide(player, [leftWall, rightWall]);
        if(game.global.shake){
            if(flag && (game.time.now > shakeTime + 4100)){
                flag = 0;
                this.bombSound = game.add.audio('bomb');
                this.bombSound.volume = 1.2;
                this.bombSound.play();
            }
            if(game.time.now > shakeTime + 5000){
                shakeTime = game.time.now;
                flag = 1;
                game.camera.shake(0.06, 2000);
            }
        }

        if(player.body.y < 0) {
            if(player.body.velocity.y < 0) {
                player.body.velocity.y = 0;
            }
            if(game.time.now > player.unbeatableTime) {
                this.hurtSound = game.add.audio('hurt');
                this.hurtSound.play();
                player.life -= 3;
                game.camera.flash(0xff0000, 100);
                player.unbeatableTime = game.time.now + 2000;
            }
        }
        if(player.life <= 0 || player.body.y > game.height+150) {
            gameOver();
        }

        this.physics.arcade.overlap(player, firstads, takeFirstad, null, this);

        updatePlayer();
        updatePlatforms();
        updateTextsBoard();

        createPlatforms();
        createFirstad();
        updateFirstad();
    }, // No changes
    
};
        // Delete all Phaser initialization code

var player;
var flag = 1;
var keyboard;
var lastTime = 0;
var lasttime2 = 0;
var shakeTime = 0;
var bgmusicSound;
var platforms = [];
var firstads = [];
var direct = 1;
var directCount = 0;
var leftWall; 
var ceiling;




var text1;
var text2;



var distance = 0;

function takeFirstad(player, firstad) {
    this.healSound.play();
    firstad.kill(); 
    player.life += 3;
    if(player.life>10){
        player.life = 10;
    }
}

function boundary () {
    leftWall = game.add.sprite(0, 0, 'wall');
    game.physics.arcade.enable(leftWall);
    leftWall.body.immovable = true;
    leftWall.scale.setTo(1, 1.5);

    rightWall = game.add.sprite(game.width-17, 0, 'wall');
    game.physics.arcade.enable(rightWall);
    rightWall.body.immovable = true;
    rightWall.scale.setTo(1, 1.5);

    ceiling = game.add.image(0, 0, 'ceiling');
    ceiling.scale.setTo(1.5, 1.5);
}

function createFirstad(){
    if(game.time.now > lasttime2 + 4000){
        lasttime2 = game.time.now;
        var x = Math.random()*(game.width - 30);
        var y = game.height;
        if(x<20) x = 30;
        if(x>game.width-20) x = game.width-20;
        var firstad = game.add.sprite(x, y, 'firstad');
        firstad.scale.setTo(0.2, 0.2);
        game.physics.arcade.enable(firstad);
        firstad.anchor.setTo(0.5, 0.5);
        firstads.push(firstad);
    }
    
}

function createPlatforms () {
    if(game.time.now > lastTime + 600){
        lastTime = game.time.now;
        createOnePlatform();
        distance += 1;
    }
}


function createOnePlatform () {

    var platform;
    var x = Math.random()*(game.width - 96 - 40) + 20;
    var x2 = x;
    if(x2<90){
        x2 = 90;
    } 
    if(x2>game.width-176){
        x2 = game.width-176;
    }
    if(game.global.shake){
        x = x2;
    }
    var y = game.height;
    var rand = Math.random() * 100;

    if(rand < 15) {
        platform = game.add.sprite(x, y, 'normal');
    } else if(rand <30){
        platform = game.add.sprite(x2, y, 'moving');
    } else if (rand < 45) {
        platform = game.add.sprite(x, y, 'nails');
        game.physics.arcade.enable(platform);
        platform.body.setSize(96, 15, 0, 15);
    } else if (rand < 60) {
        platform = game.add.sprite(x, y, 'conveyorLeft');
        platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
        platform.play('scroll');
    } else if (rand < 75) {
        platform = game.add.sprite(x, y, 'conveyorRight');
        platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
        platform.play('scroll');
    } else if (rand < 90) {
        platform = game.add.sprite(x, y, 'trampoline');
        platform.animations.add('jump', [4, 5, 4, 3, 2, 1, 0, 1, 2, 3], 120);
        platform.frame = 3;
    } else {
        platform = game.add.sprite(x, y, 'fake');
        platform.animations.add('turn', [0, 1, 2, 3, 4, 5, 0], 14);
    }

    game.physics.arcade.enable(platform);
    platform.body.immovable = true;
    platforms.push(platform);

    platform.body.checkCollision.down = false;
    platform.body.checkCollision.left = false;
    platform.body.checkCollision.right = false;
}

function playerMake() {
    player = game.add.sprite(200, 50, 'player');
    player.direction = 10;

    var platform = game.add.sprite(200, 300, 'stone');
    game.physics.arcade.enable(platform);
    platform.body.immovable = true;
    platforms.push(platform);
    platform.body.checkCollision.down = false;
    platform.body.checkCollision.left = false;
    platform.body.checkCollision.right = false;

    game.physics.arcade.enable(player);
    player.body.gravity.y = 500;
    player.animations.add('left', [0, 1, 2, 3], 8);
    player.animations.add('right', [9, 10, 11, 12], 8);
    player.animations.add('flyleft', [18, 19, 20, 21], 12);
    player.animations.add('flyright', [27, 28, 29, 30], 12);
    player.animations.add('fly', [36, 37, 38, 39], 12);
    player.life = 10;
    player.unbeatableTime = 0;
    player.touchOn = undefined;
}

function text1make () {
    var style = {fill: '#ff0000', fontSize: '20px'}
    text1 = game.add.text(10, 10, '', style);
    text2 = game.add.text(game.width-50, 10, '', style);
}

function updatePlayer () {
    if(keyboard.left.isDown) {
        player.body.velocity.x = -250;
    } else if(keyboard.right.isDown) {
        player.body.velocity.x = 250;
    } else {
        player.body.velocity.x = 0;
    }
    setPlayerAnimate(player);
}

function setPlayerAnimate(player) {
    var x = player.body.velocity.x;
    var y = player.body.velocity.y;

    if (x < 0 && y > 0) {
        player.animations.play('flyleft');
    }
    if (x > 0 && y > 0) {
        player.animations.play('flyright');
    }
    if (x < 0 && y == 0) {
        player.animations.play('left');
    }
    if (x > 0 && y == 0) {
        player.animations.play('right');
    }
    if (x == 0 && y != 0) {
        player.animations.play('fly');
    }
    if (x == 0 && y == 0) {
      player.frame = 8;
    }
}

function updatePlatforms () {
    for(var i=0; i<platforms.length; i++) {
        var platform = platforms[i];
        platform.body.position.y -= 3;
        if(platform.key == 'moving'||game.global.shake==1){
            if(direct){
                platform.body.x -= 1;
                directCount++;
            }    
            else{
                platform.body.x += 1;
                directCount--;
            }
            if(directCount==250){
                direct = 0;
                
            }else if(directCount==0){
                direct = 1;
            } 
        }
        if(platform.body.position.y <= -20) {
            platform.destroy();
            platforms.splice(i, 1);
        }
    }
}

function updateFirstad(){
    for(var i=0; i<firstads.length; i++) {
        var firstad = firstads[i];
        firstad.body.position.y -= 3;
        if(firstad.body.position.y <= -20) {
            firstad.destroy();
            firstads.splice(i, 1);
        }
    }
}

function updateTextsBoard () {
    text1.setText('life:' + player.life);
    text2.setText('B' + distance);
}

function effect(player, platform) {
    if(platform.key == 'moving') {
        if (player.touchOn !== platform) {
            player.touchOn = platform;
        }
    }
    if(platform.key == 'conveyorRight') {
        player.body.x += 2;
    }
    if(platform.key == 'conveyorLeft') {
        player.body.x -= 2;
    }
    if(platform.key == 'trampoline') {
        platform.animations.play('jump');
        this.jumpSound = game.add.audio('jump');
        this.jumpSound.play();
        player.body.velocity.y = -350;
    }
    if(platform.key == 'nails') {
        if (player.touchOn !== platform) {
            this.hurtSound = game.add.audio('hurt');
            this.hurtSound.play();
            player.life -= 3;
            player.touchOn = platform;
            game.camera.flash(0xff0000, 100);
        }
    }
    if(platform.key == 'normal') {
        if (player.touchOn !== platform) {
            player.touchOn = platform;
        }
    }
    if(platform.key == 'fake') {
        if(player.touchOn !== platform) {
            platform.animations.play('turn');
            setTimeout(function() {
                platform.body.checkCollision.up = false;
            }, 100);
            player.touchOn = platform;
        }
    }
}



function gameOver () {
    bgmusicSound.stop();
    this.dieSound = game.add.audio('die');
    this.dieSound.play();
    platforms.forEach(function(s) {s.destroy()});
    platforms = [];
    firstads.forEach(function(s) {s.destroy()});
    firstads = [];
    game.global.score = distance;
    distance = 0;
    game.state.start('gameover');
}






