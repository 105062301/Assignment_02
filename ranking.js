var rankingState = {
    preload: function(){
        if(game.global.eat){
            var dataRef = firebase.database().ref('list');
            dataRef.once('value',function(snapshot){
                var Data = snapshot.val();
                var rank = [];
                var name = [];
                var place = 6;
                rank[1] = Data.first;
                rank[2] = Data.second;
                rank[3] = Data.third;
                rank[4] = Data.fourth;
                rank[5] = Data.fifth;

                name[1] = Data.firstn;
                name[2] = Data.secondn;
                name[3] = Data.thirdn;
                name[4] = Data.fourthn;
                name[5] = Data.fifthn;
                
                for(var i=1; i<6; i++){
                    if(rank[i]){
                        if(rank[i]<game.global.score){
                            place = i;
                            break;
                        }
                    }
                    else{
                        place = i;
                        break;
                    }     
                }
                for(var j = 5; j>=place; j--){
                    rank[j+1] = rank[j];
                    name[j+1] = name[j];
                }
                rank[place] = game.global.score;
                name[place] = game.global.name;

                var scoreLabel = game.add.text(game.width/2, game.height/2-200,
                ' Rank  Name    Score' , { font: '40px Arial', fill: '#ffffff' });
                scoreLabel.anchor.setTo(0.5, 0.5);
        
                for(var i=0; i<5; i++){
                    var scoreLabel = game.add.text(200, game.height/2-150+50*i,
                    '     No.'+(i+1)+'     '+ name[i+1] +'            ' + rank[i+1], { font: '30px Arial', fill: '#ffffff' });
                    scoreLabel.anchor.setTo(0.5, 0.5);
                }

                var dataRef = firebase.database().ref('list');
                dataRef.set({
                    first: rank[1],
                    second: rank[2],
                    third: rank[3],
                    fourth: rank[4],
                    fifth: rank[5],

                    firstn: name[1],
                    secondn: name[2],
                    thirdn: name[3],
                    fourthn: name[4],
                    fifthn: name[5]
        
            })
            /*.then(function (snapshot) {
                var dataRef = firebase.database().ref('list');
                dataRef.once('value')
                .then(function (snapshot) {
                    var Data = snapshot.val();
                    var rank = [];
                    var name = [];
                    var place = 6;
                    rank[1] = Data.first;
                    rank[2] = Data.second;
                    rank[3] = Data.third;
                    rank[4] = Data.fourth;
                    rank[5] = Data.fifth;

                    name[1] = Data.firstn;
                    name[2] = Data.secondn;
                    name[3] = Data.thirdn;
                    name[4] = Data.fourthn;
                    name[5] = Data.fifthn;

                    var scoreLabel = game.add.text(game.width/2, game.height/2-200,
                    ' Rank  Name    Score' , { font: '40px Arial', fill: '#ffffff' });
                    scoreLabel.anchor.setTo(0.5, 0.5);
            
                    for(var i=0; i<5; i++){
                        var scoreLabel = game.add.text(200, game.height/2-150+50*i,
                        '     No.'+(i+1)+'     '+ name[i+1] +'            ' + rank[i+1], { font: '30px Arial', fill: '#ffffff' });
                        scoreLabel.anchor.setTo(0.5, 0.5);
                    }
                    
            
            })*/
            //.catch(e => console.log(e.message));
               // });
            })
        }
        else{
            var dataRef = firebase.database().ref('list');
                dataRef.once('value')
                .then(function (snapshot) {
                    var Data = snapshot.val();
                    var rank = [];
                    var name = [];
                    var place = 6;
                    rank[1] = Data.first;
                    rank[2] = Data.second;
                    rank[3] = Data.third;
                    rank[4] = Data.fourth;
                    rank[5] = Data.fifth;

                    name[1] = Data.firstn;
                    name[2] = Data.secondn;
                    name[3] = Data.thirdn;
                    name[4] = Data.fourthn;
                    name[5] = Data.fifthn;

                    var scoreLabel = game.add.text(game.width/2, game.height/2-200,
                    ' Rank  Name    Score' , { font: '40px Arial', fill: '#ffffff' });
                    scoreLabel.anchor.setTo(0.5, 0.5);
            
                    for(var i=0; i<5; i++){
                        var scoreLabel = game.add.text(200, game.height/2-150+50*i,
                            '     No.'+(i+1)+'     '+ name[i+1] +'            ' + rank[i+1], { font: '30px Arial', fill: '#ffffff' });
                        scoreLabel.anchor.setTo(0.5, 0.5);
                    }
                    
            
            })
            .catch(e => console.log(e.message));
            
        }
            
    },
    create: function() {
       

        // Add a background image
        this.bg = game.add.image(0, 0, 'flame');
        this.bg.scale.setTo(2.3, 3.5);
        
       
        this.button = game.add.button(game.width/2-70, game.height/2+130, 'button', this.menu, this, 2, 1, 0);
        this.button.scale.setTo(0.4, 0.4);
        var buttonLabel = game.add.text(game.width/2-50, game.height/2+130, 'Menu', { font: '25px Arial', fill: 'white' });

       
        },
        menu: function() {
            game.global.score = 0;
            game.global.eat = 0;
            game.state.start('menu');
        },
}; 



