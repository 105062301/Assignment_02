var loadState = {
    preload: function () {
    // Add a 'loading...' label on the screen
    var loadingLabel = game.add.text(game.width/2, 150, 'loading...', { font: '30px Arial', fill: '#ffffff' });
    loadingLabel.anchor.setTo(0.5, 0.5);
    // Display the progress bar
    var progressBar = game.add.sprite(game.width/2, 200, 'progressBar');
    progressBar.scale.setTo(0.5, 0.5);
    progressBar.anchor.setTo(0.5, 0.5);
    game.load.setPreloadSprite(progressBar);
    // Load all game assets
    game.load.crossOrigin = 'anonymous';
    game.load.spritesheet('player', 'assets/player.png', 32, 32);
    game.load.audio('heal', 'music/firstad.ogg');
    game.load.audio('bgmusic', 'music/background.wav');
    game.load.audio('die', 'music/die.wav');
    game.load.audio('explosion', 'music/explosion.wav');
    game.load.audio('jump', 'music/jump.wav');
    game.load.audio('hurt', 'music/hurt.wav');
    game.load.audio('bomb', 'music/bomb.wav');
    game.load.image('wall', 'assets/wall.png');
    game.load.image('ceiling', 'assets/ceiling.png');
    game.load.image('moving', 'assets/normal.png');
    game.load.image('stone', 'assets/00.png');
    game.load.image('normal', 'assets/normal.png');
    game.load.image('nails', 'assets/nails.png');
    game.load.image('night', 'assets/night.png');
    game.load.image('button', 'assets/button.png');
    game.load.spritesheet('conveyorRight', 'assets/conveyor_right.png', 96, 16);
    game.load.spritesheet('conveyorLeft', 'assets/conveyor_left.png', 96, 16);
    game.load.spritesheet('trampoline', 'assets/trampoline.png', 96, 22);
    game.load.spritesheet('fake', 'assets/fake.png', 96, 36);
    game.load.spritesheet('firstad', 'assets/firstad.png');
    // Load a new asset that we will use in the menu state
    game.load.image('background', 'assets/wall.jpg');
    game.load.image('flame', 'assets/flame.jpg');
    game.load.image('ggbackground', 'assets/gg.png');
    },
    create: function() {
    // Go to the menu state
    game.state.start('menu');
    }
}; 
