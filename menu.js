var menuState = {
    create: function() {
    // Add a background image
    this.bg = game.add.image(0, 0, 'background');
    this.bg.scale.setTo(0.4, 0.3);
    // Display the name of the game
    var nameLabel = game.add.text(game.width/2, 80, 'Downstairs',{ font: '50px Arial', fill: '#ffffff' });
    nameLabel.anchor.setTo(0.5, 0.5);
    
    // Explain how to start the game

    
    
    this.button = game.add.button(game.width/2-65, game.height/2-60, 'button', this.start, this, 2, 1, 0);
    this.button.scale.setTo(0.6, 0.4);
    var buttonLabel = game.add.text(game.width/2-32, game.height/2-58, '1 Player', { font: '25px Arial', fill: 'white' });

    this.button = game.add.button(game.width/2-65, game.height/2, 'button', this.start2, this, 2, 1, 0);
    this.button.scale.setTo(0.6, 0.4);
    var buttonLabel = game.add.text(game.width/2-32, game.height/2+2, '2 Players', { font: '25px Arial', fill: 'white' });

    this.button = game.add.button(game.width/2-65, game.height/2+60, 'button', this.start4, this, 2, 1, 0);
    this.button.scale.setTo(0.6, 0.4);
    var buttonLabel = game.add.text(game.width/2-32, game.height/2+62, 'Bombing', { font: '25px Arial', fill: 'white' });

    this.button = game.add.button(game.width/2-65, game.height/2+120, 'button', this.start3, this, 2, 1, 0);
    this.button.scale.setTo(0.6, 0.4);
    var buttonLabel = game.add.text(game.width/2-32, game.height/2+122, 'Ranking', { font: '25px Arial', fill: 'white' });

    //var startLabel = game.add.text(game.width/2, game.height-60, 'press enter key to start', { font: '25px Arial', fill: '#ffffff' });
    //startLabel.anchor.setTo(0.5, 0.5);
    // Create a new Phaser keyboard variable: the up arrow key
    // When pressed, call the 'start'
    //var enterKey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
    //enterKey.onDown.add(this.start, this);
    },
    start: function() {
    // Start the actual game
        game.global.score = 0;
        game.global.shake = 0;
        game.state.start('play');
    },
    start2: function() {
    // Start the actual game
        game.global.score = 0;
        game.global.shake = 0;
        game.state.start('play2');
    },

    start3: function() {
        // Start the actual game
        game.global.shake = 0;
        game.state.start('ranking');
        game.global.eat = 0;
    },

    start4: function() {
        // Start the actual game
        game.global.eat = 0;
        game.global.shake = 1;
        game.state.start('play');
    }
}; 



