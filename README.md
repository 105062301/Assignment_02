# Software Studio 2018 Spring Assignment 02 小朋友下樓梯

## 小朋友下樓梯
<img src="example01.png" width="700px" height="500px"></img>

## Goal
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Complete a game "小朋友下樓梯" by Phaser. (JavaScript or TypeScript)
3. Your game should reach the basic requirements.
4. You can download needed materials from some open source webpage to beautify the appearance.
5. Commit to "your" project repository and deploy to Gitlab page.
6. **Report which items you have done and describing other functions or feature in REABME.md.**

## Scoring 
|                                              Item                                              | Score |
|:----------------------------------------------------------------------------------------------:|:-----:|
| A complete game process: start menu => game view => game over => quit or play again            |  20%  |
| Your game should follow the basic rules of  "小朋友下樓梯".                                    |  15%  |
|         All things in your game should have correct physical properties and behaviors.         |  15%  |
| Set up some interesting traps or special mechanisms. .(at least 2 different kinds of platform) |  10%  |
| Add some additional sound effects and UI to enrich your game.                                  |  10%  |
| Store player's name and score in firebase real-time database, and add a leaderboard to your game.        |  10%  |
| Appearance (subjective)                                                                        |  10%  |
| Other creative features in your game (describe on README.md)                                   |  10%  |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/Assignment_02**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, .ts, etc.
    * source files
* **Deadline: 2018/05/24 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

#Report
| A complete game process: start menu => game view => game over => quit or play again            |  20%  |Y|
| Your game should follow the basic rules of  "小朋友下樓梯".                                    |  15%  |Y|
|         All things in your game should have correct physical properties and behaviors.         |  15%  |Y|
| Set up some interesting traps or special mechanisms. .(at least 2 different kinds of platform) |  10%  |Y|
| Add some additional sound effects and UI to enrich your game.                                  |  10%  |Y|
| Store player's name and score in firebase real-time database, and add a leaderboard to your game.  |  10%  |Y|
| Appearance (subjective)                                                                        |  10%  |Y|

以上要求都有做，規則和基本的小朋友校樓梯一樣。

1. UI:Phasor輸入小鍵盤。遊戲結束，輸入名字接著按下OK便自動跳轉至Ranking頁面。若進入前五則會登上Ranking
2. 點下Restart按鈕後，會重新開始玩家上一次玩的模式。ex.2P模式結束再按restart就會是2P模式。
3. 各種特殊platform，包含移動地板，輸送帶，尖刺地形......等等。
4. First ad kit item:遊戲中隨機出現漂浮的急救箱，吃到之後可以回復血量。(此game沒有其他回血方式)
5. 2 Player Mode:使用左右鍵和A,D鍵操控1P和2P，玩家可以從另一個玩家的頭上把別人踩下去。活得比較久的人獲勝。
6. Bombing Mode(轟炸模式):所有platform皆會左右搖晃，且間隔一定時間會有遠方投擲炸彈所造成的震動襲來!增加遊戲難度。



